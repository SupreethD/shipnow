using Android.App;
using Android.Content.PM;
using MvvmCross.Droid.Views;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace ShipNow.Droid
{
    [Activity(
		Label = "ShipNow.Droid"
		, MainLauncher = true
		, ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashScreen : MvxSplashScreenActivity
    {
        public SplashScreen()
            : base(Resource.Layout.SplashScreen)
        {
        }
    }
}