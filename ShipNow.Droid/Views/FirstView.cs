﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Views;
using ShipNow;

namespace ShipNow.Droid
{
	[Activity(Label = "View for StoreViewModel")]
	public class FirstView : MvxActivity<StoreViewModel>
	{
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			//SetContentView(Resource.Layout.FirstView);
		}

		protected override void OnViewModelSet()
		{
			SetContentView(Resource.Layout.FirstView);
		}
	}
}

