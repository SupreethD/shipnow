﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Views;

namespace ShipNow.Droid
{
	[Activity (Label = "ShipNow",Icon = "@android:color/transparent")]			
	public class HomeView : MvxActivity<HomeViewModel>
	{
		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your application here
		}

		protected override void OnViewModelSet()
		{
			SetContentView(Resource.Layout.HomeView);
		}
	}
}

