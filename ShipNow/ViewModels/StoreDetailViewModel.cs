﻿using System;
using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using Plugin.Messaging;
using System.Text.RegularExpressions;
using Plugin.ExternalMaps;

namespace ShipNow
{
	public class StoreDetailViewModel : BaseViewModel
	{
		public Store Store { get; set;}

		public string Monday{ get { return string.Format("{0} - {1}", Store.MondayOpen, Store.MondayClose); } }
		public string Tuesday{ get { return string.Format("{0} - {1}", Store.TuesdayOpen, Store.TuesdayClose); } }
		public string Wednesday{ get { return string.Format("{0} - {1}", Store.WednesdayOpen, Store.WednesdayClose); } }
		public string Thursday{ get { return string.Format("{0} - {1}", Store.ThursdayOpen, Store.ThursdayClose); } }
		public string Friday{ get { return string.Format("{0} - {1}", Store.FridayOpen, Store.FridayClose); } }
		public string Saturday{ get { return string.Format("{0} - {1}", Store.SaturdayOpen, Store.SaturdayClose); } }
		public string Sunday{ get { return string.Format("{0} - {1}", Store.SundayOpen, Store.SundayClose); } }

		public string Address1 { get {return Store.StreetAddress;}}
		public string Address2 { get { return string.Format ("{0}, {1} {2}", Store.City, Store.State, Store.ZipCode); } }

		public void Init(Store store)
		{
			this.Store = store;
		}
			
		private MvxCommand callCommand;
		public ICommand CallCommand
		{
			get
			{
					callCommand = callCommand ?? new MvxCommand(() => {
					var phoneCallTask =  MessagingPlugin.PhoneDialer;
					if (phoneCallTask.CanMakePhoneCall)
					phoneCallTask.MakePhoneCall(Store.PhoneNumber.CleanPhone());
				});

				return callCommand;
			}
		}

		private MvxCommand navigateCommand;
		public ICommand NavigateCommand
		{
			get
			{
				return navigateCommand ?? (navigateCommand = new MvxCommand(() =>
					CrossExternalMaps.Current.NavigateTo (Store.Name, Store.Latitude, Store.Longitude)));
			}
		}
	}
}

