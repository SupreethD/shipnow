﻿using System;
using System.Windows.Input;
using MvvmCross.Core.ViewModels;

namespace ShipNow
{
	public class HomeViewModel : BaseViewModel
	{
		Home home;
		public void Init()
		{
			home = new Home ();
		}

		public ICommand ShowStoresCommand
		{
			get
			{
				return new MvxCommand(() => ShowViewModel<StoreViewModel>());
			}
		}
	}
}

