﻿using System;
using MvvmCross.Core.ViewModels;

namespace ShipNow
{
	public class BaseViewModel : MvxViewModel
	{
		private bool _isLoading;
		protected IDataSource dataStore;
		public bool IsLoading
		{
			get 
			{ 
				return _isLoading; 
			}
			set 
			{ 
				_isLoading = value; 
				RaisePropertyChanged("IsLoading");
			}
		}

		public virtual void AsyncLoad()
		{

			if (IsLoading) 
			{
				return;
			}

			IsLoading = true;

		}
	}
}

