﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using MvvmCross.Core.ViewModels;
using System.Windows.Input;
using System.Linq;

namespace ShipNow
{
	public class StoreViewModel : BaseViewModel
	{
		private ObservableCollection<Store> stores { get; set;}
		public ObservableCollection<Grouping<string, Store>> StoresGrouped { get; set; }

		private MvxCommand<Store> itemSelectedCommand;

		public ObservableCollection<Store> Stores
		{
			get { return stores; }
			set
			{
				stores = value;
				RaisePropertyChanged("Store");
			}
		}

		StoreTableManager storeDatabase;
	
		public StoreViewModel (IDataSource service)
		{
			dataStore = service;
			Stores = new ObservableCollection<Store> ();
			storeDatabase = new StoreTableManager ();
			StoresGrouped = new ObservableCollection<Grouping<string, Store>> ();
		}
			
		public void Init()
		{
			this.AsyncLoad();
		}

		public override async void AsyncLoad()
		{
			base.AsyncLoad ();

			try
			{
				Stores.Clear();

				var stores = await dataStore.GetStoresAsync ();
				foreach(var store in stores)
				{
					if(string.IsNullOrWhiteSpace(store.Image))
						store.Image = "http://refractored.com/images/wc_small.jpg";

					Stores.Add(store);
				}

				this.Sort();

				int res = storeDatabase.SaveAllStores(Stores);

				//IEnumerable<Store> stor = storeDatabase.GetItems();

				System.Diagnostics.Debug.WriteLine (res);
			}
			catch(Exception ex) 
			{
				System.Diagnostics.Debug.WriteLine ("exception" + ex);
			}
			finally 
			{
				IsLoading = false;
			}
		}

		public ICommand ShowStoreDetailCommand
		{
			get
			{
				itemSelectedCommand = itemSelectedCommand ?? new MvxCommand<Store>(DoSelectItem);
				return itemSelectedCommand;
			}
		}

		private void DoSelectItem(Store store)
		{
			ShowViewModel<StoreDetailViewModel>(store);
		}

		private void Sort()
		{
			StoresGrouped.Clear();
			var sorted = from store in Stores
				orderby store.Country, store.City 
			group store by store.Country into storeGroup
			select new Grouping<string, Store>(storeGroup.Key, storeGroup);

			foreach(var sort in sorted)
				StoresGrouped.Add(sort);
		}
	}

	public class Grouping<K, T> : ObservableCollection<T>
	{
		public K Key { get; private set; }

		public Grouping(K key, IEnumerable<T> items)
		{
			Key = key;
			foreach (var item in items)
				this.Items.Add(item);
		}
	}
}

