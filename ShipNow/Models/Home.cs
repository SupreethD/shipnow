﻿using System;
using MvvmCross.Core.ViewModels;
using System.Windows.Input;

namespace ShipNow
{
	public class Home
	{
		public string Title{ get; set; } = "ShipNow";
		public string OptionOneText { get; set; } = "Stores";
		public string OptionTwoText { get; set; } = "Feedback";
	} 
}

