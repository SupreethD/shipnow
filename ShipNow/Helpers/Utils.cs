﻿using System;
using System.Text.RegularExpressions;

namespace ShipNow
{
	public static class Utils
	{
		private static Regex digitsOnly = new Regex(@"[^\d]");

		public static string CleanPhone(this string phone)
		{
			return digitsOnly.Replace(phone, string.Empty);
		}
	}
}

