﻿using System;
using SQLite.Net;
using System.Collections.Generic;
using System.Linq;
using SQLite.Net.Interop;
using SQLite.Net.Async;

namespace ShipNow
{
	public class StoreTableManager
	{
		static object locker = new object ();

		SQLiteConnection database;

		public StoreTableManager()
		{
			if (App.databaseConnection != null) 
			{
				database = App.databaseConnection;
			}
			// create the tables
			database.CreateTable<Store>();

		}

		public int SaveAllStores(IEnumerable<Store> data)
		{
			try
			{
				if (database.InsertAll(data) != 0)
					return database.UpdateAll(data);
				return 0;
			}
			catch (SQLiteException ex)
			{
				System.Diagnostics.Debug.WriteLine ("exception" + ex);
				return 0;
			}
		}

		public Store GetItem (string id) 
		{
			lock (locker) {
				return database.Table<Store>().FirstOrDefault(x => x.Id == id);
			}
		}

		public IEnumerable<Store> GetItems ()
		{
			lock (locker) {
				return (from i in database.Table<Store>() select i).ToList();
			}
		}
	}
}

