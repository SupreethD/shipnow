﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using PCLStorage;
using Plugin.EmbeddedResource;
using Newtonsoft.Json;
using System.Reflection;

namespace ShipNow
{
	public class OfflineDataService : IDataSource
	{
		public Task Init()
		{
			return Task.Run(() => { });
		}

		public async Task<IEnumerable<Store>> GetStoresAsync()
		{
			var rootFolder = FileSystem.Current.LocalStorage;
			var json = ResourceLoader.GetEmbeddedResourceString(Assembly.Load(new AssemblyName("ShipNow")), "stores.json");
			return await Task.Run(() => JsonConvert.DeserializeObject<List<Store>>(json));
		}
	}
}

