﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace ShipNow
{
	public interface IDataSource
	{
		Task Init();
		Task<IEnumerable<Store>> GetStoresAsync();
	}
}

