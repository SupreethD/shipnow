﻿using System;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.IoC;
using SQLite.Net;
using SQLite.Net.Interop;


namespace ShipNow
{
	public class App : MvxApplication
	{
		static string dbPath;

		static ISQLitePlatform platform;

		public static SQLiteConnection databaseConnection;

		public override void Initialize()   
		{   
		   CreatableTypes()   
		       .EndingWith("Service")   
		       .AsInterfaces()   
		       .RegisterAsLazySingleton();   
		          
			RegisterAppStart<HomeViewModel>(); 
		} 

		public static void SQLiteDBPathWithPlatform(string path , ISQLitePlatform osPlatform)
		{
			dbPath = path;
			platform = osPlatform;
			databaseConnection = new SQLiteConnection (platform, dbPath);
		}
			
	}
}

