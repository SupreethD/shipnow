﻿using System;

using Foundation;
using UIKit;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.Binding.BindingContext;
using ShipNow;

namespace ShipNow.iOS
{
	public partial class StoreViewCell : MvxTableViewCell
	{
		public static readonly NSString Key = new NSString ("StoreViewCell");
		public static readonly UINib Nib;

		static StoreViewCell ()
		{
			Nib = UINib.FromName ("StoreViewCell", NSBundle.MainBundle);
		}

		public StoreViewCell (IntPtr handle) : base (handle)
		{
			this.DelayBind(() => {
				var imageLoader = new MvxImageViewLoader(() => this.imageView);
				var set = this.CreateBindingSet<StoreViewCell,Store> ();
				set.Bind (nameLabel).To((Store vm) => vm.Name);
				set.Bind (countryNameLabel).To((Store vm) => vm.Country);
				set.Bind (imageLoader).To((Store vm) => vm.ImageUri);
				set.Apply ();
			});
		}
	}
}