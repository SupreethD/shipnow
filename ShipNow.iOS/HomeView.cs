﻿using System;
using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using ShipNow;
using MvvmCross.Binding.iOS.Views;

using UIKit;

namespace ShipNow.iOS
{
	public partial class HomeView : MvxViewController
	{
		public HomeView () : base ("HomeView", null)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			var set = this.CreateBindingSet<HomeView, HomeViewModel>();
			set.Bind(button1).To(vm => vm.ShowStoresCommand);
			set.Bind(button2).To(vm => vm.ShowStoresCommand);
			set.Apply();
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}


