﻿
using MvvmCross.iOS.Platform;
using MvvmCross.iOS.Views.Presenters;
using MvvmCross.Core.ViewModels;
using ShipNow;
using System.IO;
using System;


namespace ShipNow.iOS
{
	public class Setup : MvxIosSetup
	{
		public Setup(MvxApplicationDelegate appDelegate, IMvxIosViewPresenter presenter)
			: base(appDelegate, presenter)
		{

			var dbName = "TodoSQLite.db3";
			var documents = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			var dbPath = Path.Combine(documents, dbName);

			App.SQLiteDBPathWithPlatform (dbPath,new SQLite.Net.Platform.XamarinIOS.SQLitePlatformIOS ());

		}

		protected override IMvxApplication CreateApp ()
		{
			return new App();
		}
	}
}

