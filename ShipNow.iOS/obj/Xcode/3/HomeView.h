// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import <Foundation/Foundation.h>
#import <MvvmCross/MvvmCross.h>
#import <UIKit/UIKit.h>


@interface HomeView : UIViewController {
	UIButton *_button1;
	UIButton *_button2;
	UINavigationItem *_navigationItem;
}

@property (nonatomic, retain) IBOutlet UIButton *button1;

@property (nonatomic, retain) IBOutlet UIButton *button2;

@property (nonatomic, retain) IBOutlet UINavigationItem *navigationItem;

@end
