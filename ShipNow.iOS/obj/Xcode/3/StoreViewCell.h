// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import <Foundation/Foundation.h>
#import <MvvmCross/MvvmCross.h>
#import <UIKit/UIKit.h>


@interface StoreViewCell : UITableViewCell {
	UILabel *_countryNameLabel;
	UIImageView *_imageView;
	UILabel *_nameLabel;
}

@property (nonatomic, retain) IBOutlet UILabel *countryNameLabel;

@property (nonatomic, retain) IBOutlet UIImageView *imageView;

@property (nonatomic, retain) IBOutlet UILabel *nameLabel;

@end
