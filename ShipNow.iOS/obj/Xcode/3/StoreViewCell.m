// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import "StoreViewCell.h"

@implementation StoreViewCell

@synthesize countryNameLabel = _countryNameLabel;
@synthesize imageView = _imageView;
@synthesize nameLabel = _nameLabel;

@end
