﻿using UIKit;

using System.Collections.Generic;
using MvvmCross.iOS.Views;
using MvvmCross.Binding.iOS.Views;
using Foundation;
using MvvmCross.Binding.BindingContext;
using ShipNow;
using MvvmCross.Core.ViewModels;

namespace ShipNow.iOS
{
	public partial class StoreView : MvxViewController<StoreViewModel>
	{
		public StoreView () : base ("StoreView", null)
		{
		}

		public override void ViewDidLoad()
		{

			base.ViewDidLoad ();
			this.Title = "Locations";
			//this.CreateBinding(TipLabel).To((TipViewModel vm) => vm.Tip).Apply();

//			var source = new MvxStandardTableViewSource(storeTableView, "TitleText Name");
//			this.CreateBinding(source).To((StoreViewModel vm) => vm.Stores).Apply();
//			storeTableView.Source = source;
//			storeTableView.ReloadData();

			var source = new MvxSimpleTableViewSource(storeTableView,StoreViewCell.Key,StoreViewCell.Key);
						this.CreateBinding(source).To((StoreViewModel vm) => vm.Stores).Apply();
						storeTableView.Source = source;
						storeTableView.ReloadData();
			storeTableView.RowHeight = 81;
//			this.AddBindings(new Dictionary<object, string>
//				{
//					{source, "ItemsSource Stores"}
//				});
//
//			storeTableView.Source = source;
//			storeTableView.ReloadData();
		}
			

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}
}

}
