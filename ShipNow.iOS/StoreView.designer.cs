// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace ShipNow.iOS
{
	[Register ("StoreView")]
	partial class StoreView
	{
		[Outlet]
		UIKit.UITableView storeTableView { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (storeTableView != null) {
				storeTableView.Dispose ();
				storeTableView = null;
			}
		}
	}
}
