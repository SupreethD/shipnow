// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace ShipNow.iOS
{
	[Register ("StoreViewCell")]
	partial class StoreViewCell
	{
		[Outlet]
		UIKit.UILabel countryNameLabel { get; set; }

		[Outlet]
		UIKit.UIImageView imageView { get; set; }

		[Outlet]
		UIKit.UILabel nameLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (countryNameLabel != null) {
				countryNameLabel.Dispose ();
				countryNameLabel = null;
			}

			if (imageView != null) {
				imageView.Dispose ();
				imageView = null;
			}

			if (nameLabel != null) {
				nameLabel.Dispose ();
				nameLabel = null;
			}
		}
	}
}
