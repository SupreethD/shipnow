// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace ShipNow.iOS
{
	[Register ("HomeView")]
	partial class HomeView
	{
		[Outlet]
		UIKit.UIButton button1 { get; set; }

		[Outlet]
		UIKit.UIButton button2 { get; set; }

		[Outlet]
		UIKit.UINavigationItem navigationItem { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (navigationItem != null) {
				navigationItem.Dispose ();
				navigationItem = null;
			}

			if (button1 != null) {
				button1.Dispose ();
				button1 = null;
			}

			if (button2 != null) {
				button2.Dispose ();
				button2 = null;
			}
		}
	}
}
